import java.util.HashMap;
import java.util.Map;

public class Translator {

    //база переводов
   private Map<String, String> dictionary = new HashMap<>();

    /*
    Чтобы в мапе хранились несколько значений для 1 ключа, можно
     1 - создать мап с объектом, к примеру, с листом в качестве значения вместо просто строки
        static Map<String, ArrayList<String>> dictionary = new HashMap<>();
     2 - сделать мап с одномерным массивом в значениях, но я так понимаю, придется перезаписывать массив
     (ну и он не такой удобный, как лист)
         String[] test = {text,value,example};
         static Map<String, String[]> dictionary2 = new HashMap<>();
     */


    //добавить в базу слово с переводом
    public String addWord(String eng, String rus) {
        dictionary.put(eng, rus);
        return "В словарь добавлено слово \"" + eng + "\" с переводом \"" + dictionary.get(eng) + "\"";
    }


    //получить русский перевод для английского слова
    public String getWord(String eng) {
        dictionary.get(eng);
        return "Перевод слова \"" + eng + "\" - \"" + dictionary.get(eng) + "\"";
    }


    //удалить из базы слово с переводом
    public String deleteWord(String eng) {
        dictionary.remove(eng);
        return "Из словаря удалено  слово " + eng;
    }

    public boolean containsWord(String eng) {
        return dictionary.containsKey(eng);
    }
}
