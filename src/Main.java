import java.util.Scanner;

public class Main {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Translator englishRussianDictionary = new Translator();

        englishRussianDictionary.addWord("trick","шутка");
        englishRussianDictionary.addWord("cat","кошка");
        englishRussianDictionary.addWord("candy","конфета");
        englishRussianDictionary.addWord("pumpkin","тыква");
        englishRussianDictionary.addWord("treat","угощение");
        englishRussianDictionary.addWord("vampire","вампир");
        englishRussianDictionary.addWord("witch","ведьма");
        englishRussianDictionary.addWord("cider","сидр");
        englishRussianDictionary.addWord("ghost","призрак");
        englishRussianDictionary.addWord("cauldron","котел");
        englishRussianDictionary.addWord("chips","чипсы");

        while (true) {
            System.out.println("""
                    Доступен словарь к работе! Укажите интересующее действие. \s
                     Перевести слово - введите 1\s
                     Добавить слово - введите 2\s
                     Удалить слово - введите 3\s
                     Для завершения работы введите 4""");

            String userRequest = scanner.nextLine();

            switch (userRequest) {
                case "1":
                    System.out.println("Введите слово на английском: ");
                    String engWorld = scanner.nextLine();
                    if (englishRussianDictionary.containsWord(engWorld)) {
                        System.out.println(englishRussianDictionary.getWord(engWorld));
                    } else throw new RuntimeException("Слово не найдено в словаре, попробуйте еще раз!");
                    break;
                case "2":
                    System.out.println("Введите английское слово: ");
                    String eng = scanner.nextLine();
                    System.out.println("Введите русский перевод: ");
                    String rus = scanner.nextLine();
                    System.out.println(englishRussianDictionary.addWord(eng, rus));
                    break;
                case "3":
                    System.out.println("Введите удаляемое английское слово: ");
                    String engDel = scanner.nextLine();
                    System.out.println((englishRussianDictionary.deleteWord(engDel)));
                    break;
                case "4":
                    break;
                default:
                    throw new RuntimeException("Введено невалидное значение!");
            }

        }
    }
}